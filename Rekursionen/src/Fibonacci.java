import AlgoTools.IO;
public class Fibonacci {

	public static void main(String[] args) {

		titel("Fibonacci - Tabelle");
		
		for (int i=0; i<=15; i++) {
			IO.print("\t\t");
			IO.print(i,6);
			IO.print(" ... ");
			IO.println(fibo1(i),13);
		}
		verabschiedung();

	}

	

	private static long fibo1(int i) {
		if (i<=2)
			return 1;
		return fibo1(i-1)+fibo1(i-2);
	}



	//Titelausgabe
	static void titel(String text) {
			
		IO.print("\n\t\t");
		
		for (int i=0; i<text.length()+6; i++)
			IO.print("*");
			
		IO.print("\n\t\t*  "+ text + "  *\n\t\t");
			
		for (int i=0; i<text.length()+6; i++)
			IO.print("*");
			
		IO.println("\n");
	}
	
	
	//Ausgabe der Verabschiedung
	static void verabschiedung() {
			
		IO.println("\n\n\t Have A Nice Day!");
	}	
}
